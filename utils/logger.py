from kmonit.settings import LOG_LOCATIONS

import logging
import os
from os.path import join, isdir
from raven.contrib.django.logging import SentryHandler
from os import makedirs, chmod
import sys
import inspect


def get_logger(log_file):
    logger = None
    try:
        log_dir = LOG_LOCATIONS
        log_filename = join(log_dir, log_file)
        frame, filename, line_number, function_name, lines, index = inspect.getouterframes(inspect.currentframe())[1]
        filename = os.path.basename(filename)
        
        if not isdir(log_dir):
            makedirs(log_dir)
            chmod(log_dir, 0777)
        logger = logging.getLogger(filename)
        logger.setLevel(logging.DEBUG)
        handler = logging.handlers.RotatingFileHandler(log_filename, maxBytes = 1024000, backupCount = 100)
        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s",datefmt='%Y-%m-%d %H:%M:%S')
        handler.setFormatter(formatter)
        if SentryHandler not in map(type, logger.handlers):
            logger.addHandler(SentryHandler())
        logger.addHandler(handler)
    except Exception,e:
        logger = logging.getLogger('kmonitor')
        sys.stderr.write('error during logging setup '+ str(e))
    return logger