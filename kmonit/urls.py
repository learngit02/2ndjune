"""learn_km URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
     #url(r'^display/$','app5.views.display'),
   # url(r'^display_chart/$','app5.views.display_chart'),
    url(r'^kmonitor_chart/$','app5.views.final'),
    url(r'^m/$', 'app5.views.show_field'),
    url(r'^home/$', 'app5.views.home'),
    #url(r'^sd/$','app5.views.send_data'),
    # url(r'^display_table/$','app5.views.display')
	
]
