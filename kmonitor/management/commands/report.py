from django.core.management.base import BaseCommand, CommandError
from kmonitor.script import Monitor

class Command(BaseCommand):
    def __init__(self):
        super(Command, self).__init__()


    def handle(self, *args, **options):
        """ Method called by management command """
        obj = Monitor()
        obj.start()
    