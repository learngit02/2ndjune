import json
import urllib
import urllib2
import traceback
from string import Template
from datetime import datetime, date
from models import Reports, Reports_fields_meta_data, fields_data
from utils.logger import get_logger
import MySQLdb
import psycopg2

DEBUG = True


def debug(*arg):
    if DEBUG is True:
        print arg


class Monitor:

    def __init__(self):
        self.logger = get_logger("kmonitor.log")
        # self.check_now()

    def start(self):
        self.check_now()

    def check_now(self):
        """
        check in db, if for current time entry is present
        on report table then execute the query
        """
        now = datetime.now()
        obj = Reports.objects.filter(
            time__hour=now.hour, time__minute=now.minute)
        # obj = Reports.objects.all()
        self.logger.info(
            "obj is " + str(obj) + " " + str(now.hour) + " " + str(now.minute))
        for task in obj:
            self.task = task
            self.run_queries(task)

    def get_consolidated_results(self, task):
        now = datetime.now()
        yesterday = date.fromordinal(date.today().toordinal() - 1)
        day_before_yesterday = date.fromordinal(date.today().toordinal() - 2)
        results = fields_data.objects.filter(date__gte=datetime(
            yesterday.year, yesterday.month, yesterday.day), date__lt=datetime(now.year, now.month, now.day), fields__report=task)
        last_day_results = fields_data.objects.filter(date__gte=datetime(
            day_before_yesterday.year, day_before_yesterday.month, day_before_yesterday.day), date__lt=datetime(yesterday.year, yesterday.month, yesterday.day), fields__report=task)
        dic = self.rearrange_data(results, last_day_results)
        results = self.return_html_table(dic, day_before_yesterday, yesterday)
        return results

    def rearrange_data(self, results, last_day_results):
        dic = {}
        for row in results:
            if row.fields.id in dic.keys() and dic[row.fields.id] < row.value:
                dic[row.fields.id] = row.value
            else:
                dic[row.fields.id] = row.value
        dic2 = {}
        for row in last_day_results:
            if row.fields.id in dic2.keys() and dic2[row.fields.id] < row.value:
                dic2[row.fields.id] = row.value
            else:
                dic2[row.fields.id] = row.value

        for key in dic2.keys():
            dic2[key] = (dic2[key], dic[key])

        return dic2

    def return_html_table(self, dic, yesterday, now):
        temp = ""
        for key, value in dic.items():
            temp += "<tr><td>" + str(Reports_fields_meta_data.objects.get(id=key).name) + \
                " </td> <td>" + \
                    value[0] + "</td> <td>" + value[1] + "</td>" + \
                self.return_td_string(value) + "</tr>"
        events_dates = {"yesterday": yesterday.strftime(
            '%d/%m/%Y'), "today": now.strftime('%d/%m/%Y')}
        table = """<table border="1" cellpadding="10" cellspacing="0" width="500" height="10">
                                    <tbody>
                                        ${th}
                                        ${tr}
                                    </tbody>
                                </table>
                """
        th = """<tr><td>metrics</td><td>${today}(Yesterday)</td><td>${yesterday}</td><td>% change</td></tr>"""
        th = Template(th).substitute(events_dates)
        data = {'tr': temp, 'th': th}
        result = Template(table).substitute(data)
        return result

    def return_td_string(self, value):
        if float(self.calculate_delta(value[0], value[1])) >= 0:
            return "<td>" + str(self.calculate_delta(value[0], value[1])) + "</td>"
        else:
            return "<td bgcolor='#FF0000'>" + str(self.calculate_delta(value[0], value[1])) + "</td>"

    def calculate_delta(self, todays_data, yesterday_data):
        try:
            temp = (float(todays_data) - float(yesterday_data)) / float(todays_data)
            return format(temp * 100, ".2f")
        except:
            return 0.0

    def run_queries(self, task):
        today = datetime.now().strftime('%d/%m/%Y')
        queries = Reports_fields_meta_data.objects.filter(report=task)
        for each in queries:
            self.run_query(each)
        self.results = self.get_consolidated_results(task)
        subject = Reports.objects.filter(id=task.id)[0].name
        subject = "KMonitor Daily Report: " + \
            subject + " report for " + today
        self.send_emails(subject)

    def run_query(self, query):
        """
        running query
        """
        self.logger.info("query is " + str(query.__dict__))
        self.operation = query.name
        details = query.details
        details = json.loads(details)
        if query.report_type.lower() == 'api':
            results = self.get_data_from_api(query)
        elif query.report_type.lower() in ['database', 'db', 'databases']:
            results = self.get_data_from_db(query, **details)
        if results is not None:
            self.save_result_to_db(results, query)
        else:
            self.logger.warning("result is None, we could not\
                        fetch data from API and DB.")

    def get_data_from_api(self, link, params):
        """
        method the fetch the data from API.
        """
        params = urllib.urlencode(params)
        request = urllib2.Request(link, params)
        response = urllib2.urlopen(request).read()
        self.logger.info("got data from " + response)
        return response

    def get_data_from_db(self, obj, db_vendor, db_ip_address, db_port,
                         db_user, db_password, db_name, query):
        debug("Trying to connect with db", db_vendor,
              db_user, db_password, query, db_ip_address,
              db_port, db_name)
        try:
            conn = self.database_connection(
                db_vendor, db_ip_address, int(db_port), db_user, db_password, db_name)
            cur = conn.cursor()
            cur.execute(query)
            self.results = cur.fetchall()[0][0]
            debug("results is ", self.results)
            self.logger.info("results is " + str(self.results))
            return self.results
        except:
            self.logger.exception("Exception " + str(traceback.print_exc()))
        return None

    def database_connection(self, db_vendor, db_ip_address, db_port, db_user, db_password, db_name):
        if db_vendor.lower() == "mysql":
            """
            return driver details of MySQL
            """
            db = MySQLdb.connect(host=db_ip_address,
                                 port=db_port,
                                 user=db_user,
                                 passwd=db_password,
                                 db=db_name)

        if db_vendor.lower() == "postgres":
            """
            return driver details of postgres
            """
            db = psycopg2.connect(database=db_name,
                                  user=db_user,
                                  password=db_password,
                                  host=db_ip_address,
                                  port=db_port)
        return db

    def send_emails(self, subject):
        for email in self.task.emails.split(","):
            self.send_email(
                email, subject, str(self.results))

    def send_email(self, sender_id, subject, message):
        KSERVICES_EMAIL_URL = "http://kservices.knowlarity.com/kemail/send"
        params = {
            "to_address": sender_id,
            "from_address": 'info@superreceptionist.in',
            "mime_type": "html",
            "message": message,
            "subject": subject
        }
        try:
            params = urllib.urlencode(params)
            request = urllib2.Request(KSERVICES_EMAIL_URL, params)
            urllib2.urlopen(request)
            self.logger.info("sent email to " + sender_id)
        except:
            self.logger.exception("Exception : " + str(traceback.print_exc()))

    def save_result_to_db(self, result, query):
        """
        saving result to db
        """
        obj = fields_data(value=result)
        obj.fields = query
        obj.save()
        self.logger.info("successfully saved entry to db")

    def get_result_from_db(self):
        """
        getting result from db
        """
        pass
