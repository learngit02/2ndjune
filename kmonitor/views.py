from django.shortcuts import render,render_to_response

from django.http import HttpResponse
from django.template import Context
import MySQLdb
import json
from django.http import JsonResponse
from django.http import Http404


# Create your views here.
def show_field(request):
    if request.is_ajax():
    	r=request.GET['report']
    	if r=='SIVR':
    		i=1
    	else:
    		i=2
    	db=MySQLdb.connect(user='root',db='learnkm',passwd='',host='localhost')
	cursor=db.cursor()
	cursor.execute('select name from app5_reports_fields_meta_data where report_id=%s',[i]);
	items=[]
	for row in cursor.fetchall():
		items.append(row)
        
        data = json.dumps(items)
        return HttpResponse(data, content_type='application/json')
    else:
        raise Http404
def home(request):
	return render(request,'f3.html')
	
"""def send_data(request):
	if request.method=='GET':
		r=request.GET['report']
		f=request.GET['field']
		sd=request.GET['startDate']
		ed=request.GET['endDate']
		#return render(request,'f2.html',{'r':r,'f':f,'sd':sd,'ed':ed})
		final(r,f,sd,ed)"""
	
	

def display(request):
	db=MySQLdb.connect(user='root',db='learnkm',passwd='',host='localhost')
	cursor=db.cursor()
        report="sivr"
	field="total_no_of_cdrs_recieved"
	startdate1="2016-01-08"
	startdate2="2016-01-13"
	cursor.execute("""select value,a.date from app5_fields_data as a,app5_reports as b,app5_reports_fields_meta_data as c where  b.name=%s and b.id=c.report_id and c.id=a.fields_id and c.name=%s and a.date between %s and %s""",(report,field,startdate1,startdate2));
        results=cursor.fetchall()
	html= '<table border="1"><tr><th>order</th><th>value</th><th>date</th></tr>'
    	html= html+ '<tbody>'
    	counter = 0
    	for row in results:
        	counter = counter + 1
        	value = row[0]
        	date = row[1]
        	html=html + '<tr><td>' + str(counter) + '</td><td>' + value + '</td><td>' + date.strftime("%d/%m/%Y") + '</td><td></td></tr>'
    		html=html +'</tbody>'
    	html= html + '</table>'
		#for row in results:
		#print row[0],row[1]
	cursor.close()
	return HttpResponse(html)

#def display_chart(request):
#	db_string=get_data_from_db()
	
	

"""def print_table1(filename,delimiter):
    data_lines=[]
    result=""
    with open(filename) as data_file:
        data_lines=data_file.readlines()
        for line in data_lines[:-1]:
            x, y=line.strip('\n').split(delimiter)
            result += "['"+x+"', "+y+"],\n"
        else:
            x, y=data_lines[-1].strip('\n').split(delimiter)
            result += "['"+x+"', "+y+"]"

    return result"""

def print_table(report,field,startdate1,startdate2):
	db=MySQLdb.connect(user='root',db='learnkm',passwd='',host='localhost')
	result=""
	cursor=db.cursor()
        #report="sivr"
	#field="total_no_of_cdrs_recieved"
	#startdate1="2016-01-08"
	#startdate2="2016-01-13"
	cursor.execute("""select value,a.date from app5_fields_data as a,app5_reports as b,app5_reports_fields_meta_data as c where  b.name=%s and b.id=c.report_id and c.id=a.fields_id and c.name=%s and a.date between %s and %s""",(report,field,startdate1,startdate2));
        results=cursor.fetchall()
	for row in results:
		result += "['"+row[1].strftime("%Y.%m.%d")+"',"+row[0]+"],\n"
	return result


def printHTTPheader():
    print "Content-type: text/html"
    print ""
    print ""


def final(request):
    if request.method=='GET':
	a=request.GET['report']
	b=request.GET['field']
	c=request.GET['startDate']
	d=request.GET['endDate']

    printHTTPheader()

    # this string contains the web page that will be served
    page_str="""<html><head>
    <h1>Kmonitor Chart</h1> 

    <script type="text/javascript" src="https://www.google.com/jsapi">
</script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
    ['Date','Value'],
    %s 
        ]);

        var options = {
          title: '',
	  curveType:'function',
          hAxis: {title: 'Date', titleTextStyle: {color: 'blue'}},
          vAxis: {title: 'Value', titleTextStyle: {color: 'blue'}}
        };

        var chart = new google.visualization.ColumnChart
                (document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
	</head>
	<body>
    <div id="chart_div"></div>

    </body>
    """ % print_table(a,b,c,d)

    # serve the page with the data table embedded in it
    return HttpResponse(page_str)

#if __name__=="__main__":
 #   main()	

def display_chart(request):
	db=MySQLdb.connect(user='root',db='learnkm',passwd='',host='localhost')
	cursor=db.cursor()
        report="sivr"
	field="total_no_of_cdrs_recieved"
	startdate1="2016-01-10"
	startdate2="2016-01-13"
	cursor.execute("""select value,a.date date from app5_fields_data as a,app5_reports as b,app5_reports_fields_meta_data as c where  b.name=%s and b.id=c.report_id and c.id=a.fields_id and c.name=%s and a.date between %s and %s""",(report,field,startdate1,startdate2));
        #results=cursor.fetchall()
	#list1=[]
	#for row in results:
	#	list2={
	#		'date':row[1],
	#		'value':row[0]}
	#	list1.append(list2)
	
	#return render(request,"chart.html")
        #template = get_template('mychart.html')
        #json_string=json.dumps(['hello':'vin','hello1':'vin1'])

	json_string=json.dumps(dict(cursor.fetchall()))
        #response_body = """<html>
	#	<script>(function() {
  	#	yourJsonParseFn("%s");
	#	}())</script>
	#	</html>
	#	""" % json_string
         #return HttpResponse(response_body)
	return render(request,'mychart.html',{'vineeta':json_string})
	#return render(request,"mychart.html")
        #, content_type='application/json')
	#return JsonResponse(json_string)

