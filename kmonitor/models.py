from __future__ import unicode_literals

from django.db import models

# Create your models here.


enum = (
    ('API', 'API'),
    ('DB', 'DB')
)


class Reports(models.Model):
    name = models.CharField(max_length=200)
    report_type = models.CharField(max_length=200)
    emails = models.CharField(max_length=200)
    frequency = models.IntegerField(max_length=200)
    time = models.TimeField(auto_now=True)


class Reports_fields_meta_data(models.Model):
    name = models.CharField(max_length=200)
    weightage = models.IntegerField()
    report_type = models.CharField(max_length=200, choices=enum)
    details = models.TextField()
    report = models.ForeignKey(Reports, blank=True, null=True)


class fields_data(models.Model):
    fields = models.ForeignKey(Reports_fields_meta_data, blank=True, null=True)
    date = models.DateTimeField(auto_now = True)
    value = models.TextField()
