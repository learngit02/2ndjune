run the following steps for setup:
cd /srv
git clone git@bitbucket.org:knowlarity_team/kmonitor.git
cd ~
virtualenv kmonitor 
source ~/kmonitor/bin/activate
pip install -r requirements.txt
cd /srv/kmonitor
python manage.py makemigrations kmonitor
python manage.py migrate
